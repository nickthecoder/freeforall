pluginManagement {
    val kotlin_version: String by settings
    plugins {
        kotlin("jvm") version kotlin_version apply false
        kotlin("multiplatform") version kotlin_version apply false
        kotlin("plugin.serialization") version kotlin_version apply false
    }
}

rootProject.name = "GamesCupboard"

include(":gamescupboard-server")
include(":gamescupboard-common")
include(":gamescupboard-client")
