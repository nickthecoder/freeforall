package uk.co.nickthecoder.gamescupboard.common

import kotlinx.serialization.Serializable

@Serializable
class Avatar(
    override val id: Int,
    override var x: Int,
    override var y: Int,
    val playerId: Int,
    override var draggable: Boolean = false,

    ) : GameObject {

    override val name: String
        get() = "avatar$playerId"

    /**
     * NOTE. Avatars are never private!
     */
    override var privateToPlayerId: Int? = null
    override var draggingByPlayerId: Int? = null

    /**
     * Which side of the screen. Chat messages will appear towards the center
     * (i.e. the other direction).
     * 0 = Top, 1 = Right, 2 = Bottom, 3 = Left
     */
    var side: Int = 0

    override fun copy(newId: Int) = Avatar(newId, x, y, playerId, draggable)

    override fun toString() = "Avatar#$id : player#${playerId} @ $x,$y"

}
