package uk.co.nickthecoder.gamescupboard.common

import kotlinx.serialization.Serializable

@Serializable
data class CommandInfo(
    val name: String,
    val helpText: String,
    val minParameters: Int = 0,
    val maxParameters: Int = minParameters,
    val parameterHelp: List<String> = emptyList()
)
