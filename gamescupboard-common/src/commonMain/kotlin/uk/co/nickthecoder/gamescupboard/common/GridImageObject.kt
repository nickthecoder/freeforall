package uk.co.nickthecoder.gamescupboard.common

import kotlinx.serialization.Serializable

/**
 * An image taken from a [Grid], i.e. the file contains lots of mini images arranged in a grid.
 * To specify which image we are talking about, we need to specify the location in the grid using
 * [gx] and [gy].
 */
@Serializable
class GridImageObject(
    override val id: Int,
    override var x: Int,
    override var y: Int,
    override val name: String = "",
    override var draggable: Boolean = true,

    val grid: Grid,
    val gx: Int,
    val gy: Int,

    ) : GameObject {

    override var draggingByPlayerId: Int? = null
    override var privateToPlayerId: Int? = null

    override fun copy(newId: Int) =
        GridImageObject(
            newId, x, y, name = name,
            grid = grid, gx = gx, gy = gy
        ).apply {
            draggable = this@GridImageObject.draggable
            privateToPlayerId = this@GridImageObject.privateToPlayerId
        }

    override fun toString() = "GridImage#$id : $name @ $x,$y"

}
