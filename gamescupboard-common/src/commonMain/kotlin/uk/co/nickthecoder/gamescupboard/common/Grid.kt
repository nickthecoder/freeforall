package uk.co.nickthecoder.gamescupboard.common

import kotlinx.serialization.Serializable

@Serializable
data class Grid(
    val name: String,
    val path: String,

    val across: Int,
    val down: Int,

    val itemWidth: Int,
    val itemHeight: Int
)
