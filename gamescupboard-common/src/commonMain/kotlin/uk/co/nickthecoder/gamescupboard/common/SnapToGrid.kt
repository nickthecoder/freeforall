package uk.co.nickthecoder.gamescupboard.common

import kotlinx.serialization.Serializable

@Serializable
sealed class SnapToGrid {
    abstract fun snap(x: Int, y: Int): Pair<Int, Int>
}

@Serializable
class RectangularSnapToGrid(val dx: Int, val dy: Int) : SnapToGrid() {

    override fun snap(x: Int, y: Int): Pair<Int, Int> {
        return Pair(
            (x / dx) * dx + dx / 2,
            (y / dy) * dy + dy / 2
        )
    }

}

@Serializable
class HexSnap(val dx: Double, val dy: Double) : SnapToGrid() {
    override fun snap(x: Int, y: Int): Pair<Int, Int> {
        val gy = (y / dy).toInt()
        val gx = ((x - (gy * dx / 2)) / dx).toInt()
        return if (gx < 0 || gy < 0 || gx >= 11 || gy >= 11) {
            // Do not snap
            Pair(x, y)
        } else {
            Pair((dx / 2 + gx * dx + gy * dx / 2).toInt(), (dy / 2 + gy * dy).toInt())
        }
    }
}
