package uk.co.nickthecoder.gamescupboard.common

import kotlinx.serialization.Serializable

/**
 * GameVariation has a list of [CommandPrototype]s, which are added to the Toolbar.
 * if [isComplete], then the button will run the command.
 * Otherwise, it will add the command to the chatInput, and allow the use to complete the command.
 */
@Serializable
data class CommandPrototype(
    val label: String,
    val commandName: String,
    val parameters: List<String> = emptyList(),
    val isComplete: Boolean = false,
    /**
     * Named objects which will run this command when double-clicked.
     * e.g. The names of Dice, which will run a "Roll Dice" command.
     */
    val objectNames: List<String> = emptyList()
)
