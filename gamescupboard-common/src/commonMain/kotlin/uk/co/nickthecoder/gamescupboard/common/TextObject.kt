package uk.co.nickthecoder.gamescupboard.common

import kotlinx.serialization.Serializable


enum class TextStyle {
    PLAIN, WHITE_BOX, LIGHT_BOX
}

@Serializable
class TextObject(
    override val id: Int,
    override var x: Int,
    override var y: Int,
    override val name: String = "",
    override var draggable: Boolean = true,

    var text: String,

    /**
     * Decorate the text in a specific style.
     */
    val style: TextStyle = TextStyle.PLAIN

) : GameObject {

    override var privateToPlayerId: Int? = null
    override var draggingByPlayerId: Int? = null

    val cyclicText = mutableListOf<String>()
    var cyclicIndex = 0

    override fun copy(newId: Int) = TextObject(
        newId, x, y, name,
        text = text
    ).apply {
        draggable = this@TextObject.draggable
        privateToPlayerId = this@TextObject.privateToPlayerId
    }

    override fun toString() = "Text#$id : '$text' @ $x,$y"

}

