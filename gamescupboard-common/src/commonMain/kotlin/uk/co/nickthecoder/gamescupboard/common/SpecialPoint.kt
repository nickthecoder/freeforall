package uk.co.nickthecoder.gamescupboard.common

import ChangeZOrder
import kotlinx.serialization.Serializable

/**
 * A named point on the PlayingArea.
 * Pieces can snap to this point.
 * The game objects at this point can be found by commands.
 * e.g. The "deal" command can take cards from the [SpecialPoint] named "deck", and
 * place them in the "hand" [SpecialArea] of each player.
 */
@Serializable
data class SpecialPoint(
    val name: String,
    val x: Int,
    val y: Int,

    /**
     * Should flippable objects be made face up (true) face down (false), or no change (null).
     */
    val isFaceUp: Boolean = false,

    /**
     * Game objects placed at ([x], [y]) will be placed at the bottom of the Z-Order.
     * First used for a deck of cards. Placing a card onto the deck, will put it at the bottom of the pack.
     * NOTE. It will still be placed above background images.
     */
    val changeZOrder: ChangeZOrder = ChangeZOrder.NO_CHANGE,
    /**
     * When dragging, game objects near ([x], [y]) will snap exactly to ([x], [y]).
     * To prevent snapping, use a value 0 (the default).
     */
    val snapDistance: Int = 30
) {

    /**
     * Each client keeps track of the number of objects on this point.
     * This field is unused on the server.
     *
     * Used to display the number of cards in a pile (e.g. the main deck).
     */
    var pieceCount = 0

    fun contains(x: Int, y: Int) =
        x >= this.x - snapDistance && y >= this.y - snapDistance && x < this.x + snapDistance && y < this.y + snapDistance

}
