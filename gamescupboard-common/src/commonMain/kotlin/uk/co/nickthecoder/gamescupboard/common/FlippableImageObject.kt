package uk.co.nickthecoder.gamescupboard.common

import kotlinx.serialization.Serializable

@Serializable
class FlippableImageObject(
    override val id: Int,
    override var x: Int,
    override var y: Int,
    override val name: String = "",
    override var draggable: Boolean = true,

    val grid: Grid,
    val gx: Int,
    val gy: Int,

    /**
     * If the object is flippable (e.g. a Card, then where is its alternative image?)
     */
    val altX: Int = gx,
    val altY: Int = gy,

    var isFaceUp: Boolean

) : GameObject, RegenGameObject {

    override var draggingByPlayerId: Int? = null
    override var privateToPlayerId: Int? = null
    override var regen = false
    override var isCopy = false

    override fun copy(newId: Int) =
        FlippableImageObject(
            newId, x, y, name = name,
            grid = grid, gx = gx, gy = gy, altX = altX, altY = altY,
            isFaceUp = isFaceUp
        ).apply {
            draggable = this@FlippableImageObject.draggable
            privateToPlayerId = this@FlippableImageObject.privateToPlayerId
        }

    override fun toString() = "FlippableImage#$id : '${grid.path}' @ $x,$y"

}
