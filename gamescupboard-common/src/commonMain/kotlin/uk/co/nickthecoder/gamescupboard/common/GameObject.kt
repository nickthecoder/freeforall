package uk.co.nickthecoder.gamescupboard.common

import MoveObject
import AddObjects
import RemoveObjects
import kotlinx.serialization.Serializable

/**
 * All game pieces are subclasses of [GameObject], even those that do not move.
 *
 * Note, all properties are abstract, so that subclasses can be automatically Seriablizable.
 * See https://github.com/Kotlin/kotlinx.serialization/issues/599#issuecomment-672889458
 */
@Serializable
sealed interface GameObject {
    val id: Int

    var x: Int
    var y: Int

    /**
     * Optional. Use the empty string, if you do not want a name.
     */
    val name: String

    val draggable: Boolean
    var draggingByPlayerId: Int?
    var privateToPlayerId: Int?

    fun newMessage() = AddObjects(listOf(this))
    fun moveMessage() = MoveObject(id, x, y, ChangeZOrder.NO_CHANGE)
    fun removeMessage() = RemoveObjects(listOf(id))

    fun copy(newId: Int = id): GameObject
}

sealed interface RegenGameObject : GameObject {
    /**
     * Used for counters/pieces in games like Hex and Super7.
     * Instead of having a stack of many objects, have just one,
     * and when it is moved, create a duplicate.
     * The original is reset to false (so that it won't be cloned again).
     */
    var regen: Boolean
    var isCopy: Boolean

    override fun copy(newId: Int): RegenGameObject
}
