package uk.co.nickthecoder.gamescupboard.common

import kotlinx.serialization.Serializable

/**
 * The corresponding GameObjectView will display the count of the number of [GameObject]s
 * at the [SpecialPoint] named [specialPointName].
 */
@Serializable
class PieceAtPointCounter(
    override val id: Int,
    override var x: Int,
    override var y: Int,

    var specialPointName: String

) : GameObject {

    override val draggable
        get() = false

    override var privateToPlayerId: Int?
        get() = null
        set(_) {} // Do nothing

    override val name: String = "${specialPointName}_counter"

    override var draggingByPlayerId: Int?
        get() = null
        set(_) {} // Do nothing

    override fun copy(newId: Int) = PieceAtPointCounter(
        newId, x, y, specialPointName = specialPointName
    )

    override fun toString() = "PieceCounter : '$specialPointName' @ $x,$y"

}
