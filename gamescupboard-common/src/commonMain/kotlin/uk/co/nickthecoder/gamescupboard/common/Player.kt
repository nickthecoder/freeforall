package uk.co.nickthecoder.gamescupboard.common

import kotlinx.serialization.Serializable

/**
 * Data about a player.
 * The server stores this data in a List.
 * Messages between the client and server also contain this data.
 */
@Serializable
class Player(
    val id: Int,

    var name: String,
    val color: String,
    val isSpectator: Boolean
) {

    override fun toString(): String {
        return "Player '$name' #$id ($color)"
    }

}

/**
 * Each seat has fixed colors.
 * These colors are used for the Avatar, as well as the color of names in the ChatLog.
 *
 * Note, player#0 is never used. So WHITE should NEVER be used!
 */
