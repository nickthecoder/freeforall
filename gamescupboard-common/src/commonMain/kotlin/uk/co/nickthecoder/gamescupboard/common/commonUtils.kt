package uk.co.nickthecoder.gamescupboard.common

import kotlinx.serialization.json.Json
import kotlinx.serialization.serializer

// Sizes
// Note, these are in the "Common" module, because the server needs to know the size of the playing area.

const val playingAreaWidth = 800
const val playingAreaHeight = 600

const val toolbarHeight = 46

const val dockWidth = 30.0
const val dockHeight = playingAreaHeight

const val stageWidth = playingAreaWidth + dockWidth.toInt()
const val stageHeight = playingAreaHeight + toolbarHeight

inline fun <reified T> Json.decodeOrNull(string: String): T? {
    return try {
        decodeFromString(serializersModule.serializer(), string)
    } catch (e: Exception) {
        null
    }
}
