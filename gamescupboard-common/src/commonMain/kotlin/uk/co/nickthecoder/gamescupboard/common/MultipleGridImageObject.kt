package uk.co.nickthecoder.gamescupboard.common

import kotlinx.serialization.Serializable

/**
 * Images from a [Grid]. The view displays one of the grid images.
 * First used for dice.
 */
@Serializable
class MultipleGridImageObject(
    override val id: Int,
    override var x: Int,
    override var y: Int,
    override val name: String = "",
    override var draggable: Boolean = true,

    val grid: Grid,
    val fromGX: Int,
    val toGX: Int,
    val fromGY: Int,
    val toGY: Int = fromGY,
    var imageNumber: Int = 0

) : GameObject {

    override var draggingByPlayerId: Int? = null
    override var privateToPlayerId: Int? = null

    override fun copy(newId: Int) =
        MultipleGridImageObject(
            newId, x, y, name = name,
            grid = grid,
            fromGX = fromGX, toGX = toGX,
            fromGY = fromGY, toGY = toGY,
            imageNumber = imageNumber
        ).apply {
            draggable = this@MultipleGridImageObject.draggable
            privateToPlayerId = this@MultipleGridImageObject.privateToPlayerId
        }

    override fun toString() = "MultipleGridImage#$id : '${grid.path}' @ $x,$y"

}
