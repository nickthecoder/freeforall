package uk.co.nickthecoder.gamescupboard.common

import kotlinx.serialization.Serializable

@Serializable
open class ImageObject(
    override val id: Int,
    override var x: Int,
    override var y: Int,
    override val name: String = "",
    override var draggable: Boolean = true,

    var path: String,
    var mirrorX: Boolean = false,
    var mirrorY: Boolean = false

) : GameObject, RegenGameObject {

    var scale: Double = 1.0

    override var regen = false
    override var isCopy = false

    override var draggingByPlayerId: Int? = null
    override var privateToPlayerId: Int? = null
    var isScalable = false

    override fun copy(newId: Int) =
        ImageObject(
            newId, x, y, name,
            path = path
        ).apply {
            scale = this@ImageObject.scale
            draggable = this@ImageObject.draggable
            privateToPlayerId = this@ImageObject.privateToPlayerId
        }.apply {
            isScalable = this@ImageObject.isScalable
            regen = this@ImageObject.regen
        }

    override fun toString() = "Image#$id : '$path' @ $x,$y"

}

