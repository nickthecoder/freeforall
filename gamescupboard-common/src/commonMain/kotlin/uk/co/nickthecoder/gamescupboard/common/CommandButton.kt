package uk.co.nickthecoder.gamescupboard.common

import kotlinx.serialization.Serializable

/**
 * A button which appears on the playing area.
 * Click it will run the CommandProtoType.
 * i.e. if it is a complete command [CommandPrototype.isComplete] == true, the run the command,
 * otherwise, just add the partial command to the Chat Input.
 */
@Serializable
data class CommandButton(
    val label: String,
    val commandPrototype: CommandPrototype,
    val x: Int,
    val y: Int
)
