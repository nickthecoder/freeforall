buildscript {
    repositories {
        mavenLocal()
        mavenCentral()
        google()
        maven { url = uri("https://maven.pkg.jetbrains.space/public/p/ktor/eap") }
        maven { url = uri("https://plugins.gradle.org/m2/") }
    }
}

plugins {
    kotlin("jvm") apply false
    kotlin("multiplatform") apply false
    kotlin("plugin.serialization") apply false
}

defaultTasks("gamescupboard-server:installDist")


// Ensure that the gamescupboard-client.js etc. are created too for tasks installDist and run.
// Oh dear, I don't know how to do this using "nice" DSL
tasks.findByPath(":gamescupboard-server:installDist")!!.dependsOn(
    tasks.findByPath( ":gamescupboard-client:browserReleaseEsbuild" )
)
tasks.findByPath(":gamescupboard-server:run")!!.dependsOn(
    tasks.findByPath( ":gamescupboard-client:browserReleaseEsbuild" )
)
