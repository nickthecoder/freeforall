import com.soywiz.korge.gradle.*

buildscript {
    val korge_version: String by project

    repositories {
        mavenLocal()
        mavenCentral()
        google()
        maven { url = uri("https://plugins.gradle.org/m2/") }
    }
    dependencies {
        classpath("com.soywiz.korlibs.korge.plugins:korge-gradle-plugin:$korge_version")
    }
}

apply<KorgeGradlePlugin>()

korge {
    id = "co.uk.nickthecoder.gamescupboard"
    name = "Games Cupboard"

    targetJvm()
    targetJs()

    serializationJson()

    entryPoint = "main"
    jvmMainClassName = "uk.co.nickthecoder.gamescupboard.client.MainKt"

    val ktor_version: String by project
    project.dependencies.add("commonMainImplementation", project(":gamescupboard-common"))
    project.dependencies.add("commonMainApi", "io.ktor:ktor-client-websockets:$ktor_version")
    project.dependencies.add("jvmMainApi", "io.ktor:ktor-client-cio:$ktor_version")
    project.dependencies.add("jsMainApi", "io.ktor:ktor-client-js:$ktor_version")

}
