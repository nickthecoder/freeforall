package uk.co.nickthecoder.gamescupboard.client

import com.soywiz.korio.lang.substr
import kotlinx.browser.*

actual class Parameters {

    actual companion object {
        private val map = mutableMapOf<String, String>()

        actual val host: String
            get() = window.location.hostname

        actual val port: Int
            get() = window.location.port.toIntOrNull() ?: 80

        init {
            var search = window.location.search
            if (search.firstOrNull() == '?') search = search.substr(1)
            val params = search.split("&")
            for (param in params) {
                val eq = param.indexOf('=')
                if (eq > 0) {
                    println("Put ${param.substring(0, eq)} -> '${param.substring(eq + 1)}'")
                    map[param.substring(0, eq)] = param.substring(eq + 1)
                }
            }
        }

        actual operator fun get(name: String) = map[name]
    }
}
