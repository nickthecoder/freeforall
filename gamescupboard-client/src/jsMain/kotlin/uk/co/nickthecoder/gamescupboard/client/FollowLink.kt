package uk.co.nickthecoder.gamescupboard.client

import kotlinx.browser.window

actual object FollowLink {
    actual fun follow(url: String) {
        window.open(url, "_blank")
    }
}
