package uk.co.nickthecoder.gamescupboard.client

import com.soywiz.klock.seconds
import com.soywiz.korge.tween.get
import com.soywiz.korge.tween.tween
import com.soywiz.korge.view.ClipContainer
import com.soywiz.korge.view.View
import com.soywiz.korge.view.anchor
import com.soywiz.korge.view.image
import com.soywiz.korim.bitmap.Bitmap
import com.soywiz.korim.color.Colors

/**
 * The layer above [PlayingArea]
 *
 * Contains static images, as well as the mouse highlights
 */
class ForegroundArea(
    width: Double, height: Double

) : ClipContainer(width, height) {

    private val mouseHighlights = mutableListOf<View>()

    /**
     * This should be called AFTER all foreground objects have been added,
     * so that the mouse highlights appear top-most.
     */
    fun addMouseHighlights(colors: List<String>, bitmap: Bitmap) {
        for (color in colors) {
            mouseHighlights.add(image(bitmap) {
                anchor(0.5, 0.5)
                visible = false
                tint = Colors[color]
            }
            )
        }
    }

    fun showMouseHighlight(playerId: Int, x: Int, y: Int) {
        mouseHighlights.getOrNull(playerId - 1)?.let { highlight ->
            highlight.x = x.toDouble()
            highlight.y = y.toDouble()
            highlight.visible = true
            highlight.scale = 0.1
            highlight.launchTween(highlight::scale[1.0], time = 0.5.seconds)
        }
    }

    fun hideMouseHighlight(playerId: Int) {
        mouseHighlights.getOrNull(playerId - 1)?.let { highlight ->
            highlight.launchTweenThen(highlight::scale[0.1], time = 0.5.seconds) {
                highlight.visible = false
            }
        }
    }

    fun moveMouseHighlight(playerId: Int, x: Int, y: Int) {
        mouseHighlights.getOrNull(playerId - 1)?.let { highlight ->
            highlight.x = x.toDouble()
            highlight.y = y.toDouble()
        }
    }
}
