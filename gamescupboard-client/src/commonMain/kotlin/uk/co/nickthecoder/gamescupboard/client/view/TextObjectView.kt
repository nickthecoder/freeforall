package uk.co.nickthecoder.gamescupboard.client.view

import CycleText
import com.soywiz.korge.input.doubleClick
import com.soywiz.korge.input.mouse
import com.soywiz.korge.view.*
import com.soywiz.korim.color.Colors
import com.soywiz.korim.color.RGBA
import com.soywiz.korma.geom.Point
import uk.co.nickthecoder.gamescupboard.client.send
import uk.co.nickthecoder.gamescupboard.client.stylePlainBackground
import uk.co.nickthecoder.gamescupboard.client.stylePlainColor
import uk.co.nickthecoder.gamescupboard.common.TextObject
import uk.co.nickthecoder.gamescupboard.common.TextStyle

/**
 * The view for a [TextObject].
 * It has text, as well as additional "decorations" governed by [style].
 */
open class TextObjectView(
    id: Int,
    str: String,
    private val style: TextStyle,
    val cyclicText: List<String> = emptyList()
) : GameObjectView(id) {

    private val textView = Text(str, color = textColor()).apply {
        position(-width / 2, -height / 2)
    }

    private var background = createBackground()

    var text: String
        get() = textView.text
        set(v) {
            with(textView) {
                text = v
                position(-width / 2, -height / 2)
            }
            background.removeFromParent()
            background = createBackground()
            addChildAt(background, 0)
        }

    var cyclicIndex = 0
        set(v) {
            field = v % cyclicText.size
            text = cyclicText[cyclicIndex]
        }

    init {
        addChild(background)
        addChild(textView)

        if (cyclicText.isNotEmpty()) {
            mouse.doubleClick {
                if (it.clickedCount == 2) {
                    cyclicIndex++
                    CycleText(id, cyclicIndex).send()
                }
            }
        }
    }

    private fun textColor(): RGBA {
        // Other styles will be added as needed...
        return when (style) {
            else -> stylePlainColor
        }
    }

    private fun createBackground(): View {

        // Other styles will be added as needed...
        return when (style) {
            TextStyle.WHITE_BOX -> RoundRect(
                textView.width + 10, textView.height + 10, rx = 9.0,
                fill = Colors.WHITE
            )

            TextStyle.LIGHT_BOX -> RoundRect(
                textView.width + 10, textView.height + 10, rx = 9.0,
                fill = stylePlainBackground
            )

            else -> SolidRect(
                textView.width + 4, textView.height + 4, color = Colors.TRANSPARENT_WHITE
            )
        }.apply {
            position(-width / 2, -height / 2)
        }
    }

    override fun isTouching(point: Point): Boolean {
        return point.x >= x - background.width / 2 && point.x <= x + background.width / 2 &&
                point.y >= y - background.height / 2 && point.y <= y + background.height / 2
    }
}
