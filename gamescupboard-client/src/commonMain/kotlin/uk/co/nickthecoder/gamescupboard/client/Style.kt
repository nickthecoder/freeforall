package uk.co.nickthecoder.gamescupboard.client

import com.soywiz.korge.ui.BoxUISkin
import com.soywiz.korge.ui.UISkin
import com.soywiz.korge.ui.textSize
import com.soywiz.korge.ui.uiSkinBitmap
import com.soywiz.korim.color.Colors

/*
    Colors, and size etc. of UI elements.
    I keep them all in one place, so that my code is not peppered with hard-coded values
    when would all need to be changed if/when I change color scheme.
*/

val windowBackground = Colors["#222"]

val outsideColor = Colors["#444"]

val labelColor = Colors["#ccc"]

val textColor = Colors["#eee"]

val toolbarColor = Colors["#444"]

val dockableColor = Colors["#222e"]

val dockColor = Colors["#333"]
val dockButtonColor = Colors["#333"]
val dockButtonSelectedColor = Colors["#222"]

val avatarBorderColor = Colors.BLACK
val bubbleColor = Colors["#ccc"]
val bubbleTextColor = Colors["#444"]

val warnCardWillBeVisible = Colors["#f88"]
val binHighlight = Colors["#f88"]

val helpBackground = Colors["#222e"]

val boldText = Colors["4e4"]
val underscoreText = Colors["e4ea50"]
val hyperlinks = Colors["#c6493e"]

// Not used. See GameVariation for the String representations of the standard colors.
val spectatorColor = Colors["#cec"]
val red = Colors["#c6493e"]
val blue = Colors["#465bc9"]
val orange = Colors["#c82"]
val yellow = Colors["#e4ea50"]
val green = Colors["#282"]
val magenta = Colors["#828"]

val chessBlack = Colors["#6e645b"]
val chessWhite = Colors["#efd5ba"]

/**
 * This BoxUISkin is for TextInputs, such as the chat's input field.
 */
val myBoxSkin = BoxUISkin(
    bgColor = Colors["#3c3c3c"],
    bgColorFocused = Colors["#333"],
    bgColorOver = Colors["#383838"],

    borderColor = Colors["#666"],
    borderColorFocused = Colors["#666"],
    borderColorOver = Colors["#666"],

    outlineColor = Colors["#444"]
)

/**
 * Builds a bitmap suitable for a UISkin.
 */
val mySkipBitmap = CustomisableSkinBitmapBuilder(
    lineWidth = 4,
    radius = 10,

    shapedEnabled = myBoxSkin.bgColor,
    shapedDisabled = myBoxSkin.bgColor, // I do not have any disabled controls (yet)

    normal = myBoxSkin.bgColor,
    over = myBoxSkin.bgColorOver,
    down = myBoxSkin.bgColorFocused,
    disabled = myBoxSkin.bgColor, // I do not have any disabled controls (yet)

    borderNormal = myBoxSkin.bgColor,
    borderOver = myBoxSkin.borderColorOver,
    borderDown = myBoxSkin.borderColorFocused,
    borderDisabled = myBoxSkin.bgColor, // I do not have any disabled controls (yet)

).createBitmap()

/**
 *
 */
val mySkin = UISkin {
    textSize = 14.0
    uiSkinBitmap = mySkipBitmap
}

val toolbarMargin = 8
val toolbarSpacing = 4
val toolbarButtonPadding = 8
val toolbarButtonSize = 30

val stylePlainBackground = Colors["#fff4"]
val stylePlainColor = Colors.BLACK

val avatarTextColor = Colors.BLACK
