package uk.co.nickthecoder.gamescupboard.client.view

import com.soywiz.klock.milliseconds
import com.soywiz.korge.tween.get
import com.soywiz.korge.view.image
import com.soywiz.korim.bitmap.Bitmap
import com.soywiz.korma.geom.Point
import com.soywiz.korma.interpolation.Easing
import uk.co.nickthecoder.gamescupboard.client.highlightPeriod
import uk.co.nickthecoder.gamescupboard.client.highlightScale
import uk.co.nickthecoder.gamescupboard.client.launchTween
import uk.co.nickthecoder.gamescupboard.client.launchTweenThen

open class ImageObjectView(id: Int, bitmap: Bitmap) : GameObjectView(id) {

    /**
     * The scaling excluding animation effects when you hover over this.
     */
    private var imageScale: Double = 1.0

    var isScalable = false

    var mirrorX = false
        set(v) {
            field = v
            scaleX = -imageScale
        }
    var mirrorY = false
        set(v) {
            field = v
            scaleY = -imageScale
        }

    private val image = image(bitmap) {
        x = -width / 2
        y = -height / 2
    }

    override fun defaultScale() = imageScale
    fun scaleTo(to: Double) {
        imageScale = to
        scaleX = to
        scaleY = if (mirrorY) -to else to
    }

    fun scaleBy(by: Double): Double {
        scaleTo(imageScale * by)
        return imageScale
    }

    override fun highlight() {
        launchTweenThen(
            this::scale[imageScale * highlightScale],
            easing = Easing.EASE_IN_OUT,
            time = highlightPeriod.milliseconds
        ) {
            launchTween(
                this::scale[imageScale], easing = Easing.EASE_IN_OUT, time = highlightPeriod.milliseconds
            )
        }
    }

    override fun isTouching(point: Point): Boolean {
        return point.x >= x - image.width / 2 && point.x <= x + image.width / 2 &&
                point.y >= y - image.height / 2 && point.y <= y + image.height / 2
    }
}
