package uk.co.nickthecoder.gamescupboard.client

import com.soywiz.korge.view.Container
import com.soywiz.korge.view.Text
import com.soywiz.korge.view.roundRect

fun Container.speechBubble(str: String): SpeechBubble {
    val bubble = SpeechBubble(str)
    addChild(bubble)
    return bubble
}

const val bubbleMarginX = 10
const val bubbleMarginY = 10
const val bubbleRadius = 10.0

class SpeechBubble(str: String) : Container() {

    init {
        val text = Text(str, color = bubbleTextColor).apply {
            x = -width / 2
            y = -height / 2
        }

        roundRect(
            text.width + bubbleMarginX * 2,
            text.height + bubbleMarginY * 2,
            bubbleRadius, bubbleRadius,
            bubbleColor
        ).apply {
            x = -width / 2
            y = -height / 2
        }

        addChild(text)

    }
}