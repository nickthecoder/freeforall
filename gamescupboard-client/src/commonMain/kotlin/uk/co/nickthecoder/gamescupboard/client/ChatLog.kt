package uk.co.nickthecoder.gamescupboard.client

import ChatMessage
import com.soywiz.korge.view.ClipContainer
import com.soywiz.korge.view.solidRect
import com.soywiz.korim.color.Colors
import uk.co.nickthecoder.gamescupboard.common.Player
import uk.co.nickthecoder.gamescupboard.common.dockHeight
import uk.co.nickthecoder.gamescupboard.common.stageHeight

class ChatLog : TextButtonDockable("Chat Log") {

    private val leftRightMargin = 20.0
    private val marginTop = 10.0
    private val marginBottom = 20.0

    override val panel = ClipContainer(350.0, dockHeight.toDouble()).apply {
        solidRect(width, height, dockableColor)
    }

    val out = PrintContainer(
        panel.width,
        panel.height - marginBottom,
        paddingLeft = leftRightMargin,
        paddingTop = marginTop
    ).apply {
        backgroundColor = Colors.TRANSPARENT_WHITE // Colors["#0000cc88"] //
        panel.addChild(this)
    }
    
    fun addMessage(chatMessage: ChatMessage) {
        val player = gamesCupboardClient.findPlayer(chatMessage.fromId)
        val prefix = if (player == null) {
            "<unknown> : "
        } else {
            player.name + " : "
        }
        out.print(prefix, if (player == null) textColor else Colors[player.color])
        out.println(chatMessage.str)
    }

}
