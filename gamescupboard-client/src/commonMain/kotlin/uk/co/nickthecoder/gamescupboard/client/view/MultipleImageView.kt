package uk.co.nickthecoder.gamescupboard.client.view

import com.soywiz.korge.view.image
import com.soywiz.korim.bitmap.Bitmap
import com.soywiz.korim.bitmap.BitmapSlice
import com.soywiz.korma.geom.Point

class MultipleImageView(id: Int, val images: List<BitmapSlice<Bitmap>>) : GameObjectView(id) {

    private var image = image(images[0]) {
        x = -width / 2
        y = -height / 2
    }

    var imageNumber = 0
        set(v) {
            field = v
            val oldImage = image
            image = image(images[v]) {
                x = -width / 2
                y = -height / 2
            }
            oldImage.removeFromParent()
        }

    override fun isTouching(point: Point): Boolean {
        return point.x >= x - image.width / 2 && point.x <= x + image.width / 2 &&
                point.y >= y - image.height / 2 && point.y <= y + image.height / 2
    }

    override fun toString() = "MultipleImageView @ $x, $y image#$imageNumber : $image"
}
