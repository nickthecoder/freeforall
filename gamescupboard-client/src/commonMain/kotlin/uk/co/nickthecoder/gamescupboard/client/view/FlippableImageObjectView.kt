package uk.co.nickthecoder.gamescupboard.client.view

import FaceUpOrDown
import com.soywiz.korim.bitmap.Bitmap
import com.soywiz.korim.bitmap.BitmapSlice
import uk.co.nickthecoder.gamescupboard.client.*

/**
 * Used for cards, and other objects that can be flipped over.
 *
 * Note. When cards are placed into a "private area" they are automatically made face up.
 * When dragging them out of the "private area", they will be made face down, but when the mouse is
 * released, if it is in a "public area", they will be made face up again.
 *
 */
class FlippableImageObjectView(
    id: Int,
    private val faceUpImage: BitmapSlice<Bitmap>,
    private val faceDownImage: BitmapSlice<Bitmap>,
    isFaceUp: Boolean

) : GridImageObjectView(id, if (isFaceUp) faceUpImage else faceDownImage) {

    var isFaceUp: Boolean = isFaceUp
        set(v) {
            if (field != v) {
                field = v
                if (v) {
                    image.bitmap = faceUpImage
                } else {
                    image.bitmap = faceDownImage
                }
                playSound(if (v) FACE_UP else FACE_DOWN)
            }
        }

    override fun onDoubleClick(playingArea: PlayingArea) {
        isFaceUp = ! isFaceUp
        FaceUpOrDown(id, isFaceUp).send()
    }
}
