package uk.co.nickthecoder.gamescupboard.client

import com.soywiz.korge.ui.uiButton
import com.soywiz.korge.view.Container
import com.soywiz.korge.view.RoundRect
import com.soywiz.korge.view.Text
import com.soywiz.korge.view.text
import uk.co.nickthecoder.gamescupboard.common.playingAreaWidth
import uk.co.nickthecoder.gamescupboard.common.toolbarHeight

interface IHelpArea {
    fun clear()
    fun addLabel(label: String)
    fun addButton(label: String, action: () -> Unit)
    fun show()
}

class HelpArea : IHelpArea, Container() {

    private val spacing = 10.0
    private val paddingX = 10.0
    private val paddingY = 10.0
    private val marginX = 2.0
    private val marginY = 2.0

    private var nextX = paddingX
    private var nextY = paddingY

    init {
        x = marginX
        y = toolbarHeight + marginY
    }

    override fun clear() {
        removeChildren()
        nextX = paddingX
        nextY = paddingY
        visible = false
    }

    override fun show() {
        addChildAt(
            RoundRect(
                playingAreaWidth - marginX * 2, height + paddingY * 2,
                rx = 10.0,
                fill = helpBackground
            ), 0
        )
        visible = true
        gamesCupboardClient.dock.doNotCoverPlayingArea(false)
    }

    override fun addLabel(label: String) {
        if (nextX != paddingX) {
            nextX = paddingX
            nextY += children.last().height + spacing
        }

        text(label) {
            x = nextX
            y = nextY
            nextX = paddingX
            nextY += height + spacing
        }
    }

    override fun addButton(label: String, action: () -> Unit) {
        val minWidth = 100.0
        val padding = 10.0
        uiButton(label, width = minWidth) {

            // This is a horrible bodge to set a *minimum* width for the button, but make it wider if the text is long.
            // This relies on UIButton's ordering of its children (which may change in future releases of KorGE). Arr!
            val textView = children[1] as Text
            textView.text = label
            if (textView.width + padding * 2 > minWidth) {
                width = children[1].width + padding * 2
            }

            y = nextY
            x = nextX
            nextX += width + spacing
            if (nextX > playingAreaWidth) {
                // Didn't fit. Move it onto the next line.
                x = paddingX
                y = nextY + height + spacing
                nextX = x + width + spacing
                nextY = y
            }

            onPress { action() }
        }
    }

}
