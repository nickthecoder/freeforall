package uk.co.nickthecoder.gamescupboard.client.view

import RestoreFromBin
import com.soywiz.korim.bitmap.Bitmap
import uk.co.nickthecoder.gamescupboard.client.PlayingArea
import uk.co.nickthecoder.gamescupboard.client.send

class BinView(id: Int, bitmap: Bitmap) : ImageObjectView(id, bitmap) {

    override fun onDoubleClick(playingArea: PlayingArea) {
        RestoreFromBin.send()
    }

}
