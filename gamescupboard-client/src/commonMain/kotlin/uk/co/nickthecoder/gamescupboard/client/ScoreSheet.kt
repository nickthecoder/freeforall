package uk.co.nickthecoder.gamescupboard.client

import ScoreSheetData
import com.soywiz.korge.annotations.KorgeExperimental
import com.soywiz.korge.ui.uiButton
import com.soywiz.korge.ui.uiScrollable
import com.soywiz.korge.ui.uiText
import com.soywiz.korge.view.*
import com.soywiz.korim.color.Colors
import uk.co.nickthecoder.gamescupboard.client.text.TextInput
import uk.co.nickthecoder.gamescupboard.client.text.textInput
import uk.co.nickthecoder.gamescupboard.common.dockHeight
import kotlin.math.round

/**
 * A simple table of values with a total of each column at the bottom.
 * Doesn't look nice (numbers are left aligned), but more importantly,
 * this data is only synced with the server when the Recalculate button
 * is pressed. Therefore, two people CANNOT edit the scores simultaneously.
 */
@OptIn(KorgeExperimental::class)
class ScoreSheet(val seatCount: Int, val hasBidColumn: Boolean) : TextButtonDockable("Score Sheet") {

    private val columnWidth = if (hasBidColumn) 60.0 else 100.0
    private val marginX = 10.0
    private val rowHeight = 30.0
    private val spacing = 4.0

    private val innerWidth = if (hasBidColumn) {
        seatCount * 2 * columnWidth + (seatCount * 2 - 1) * spacing
    } else {
        seatCount * columnWidth + (seatCount - 1) * spacing
    }

    override var panel = ClipContainer(innerWidth + marginX * 2 + marginX, dockHeight.toDouble())
    private val background = panel.solidRect(panel.width, panel.height, dockableColor)

    private val scrollable = panel.uiScrollable(
        panel.width - marginX,
        panel.height - if (hasBidColumn) 130 else 110
    ).apply {
        x = marginX
        y = if (hasBidColumn) 50.0 else 30.0
        backgroundColor = Colors.TRANSPARENT_WHITE
    }
    private val scrollArea = scrollable.container

    private val playerNames = mutableListOf<Text>()
    private val rows = mutableListOf<Row>()

    private val totals = panel.container {
        alignTopToBottomOf(scrollable)
        y += 10
    }

    init {
        val addRowButton = panel.uiButton("Add Row") {
            y = totals.y + 24
            onPress {
                addRow()
            }
        }
        val recalculateButton = panel.uiButton("Total & Send") {
            y = totals.y + 24
            onPress {
                recalculateAndSend()
            }
        }
        val buttonSpace = panel.width - addRowButton.width - recalculateButton.width
        addRowButton.x = buttonSpace / 3
        recalculateButton.x = panel.width - recalculateButton.width - buttonSpace / 3

        // Create the table headings (player names)
        for (i in 0 until seatCount) {
            val j = if (hasBidColumn) i * 2 + 1 else i

            val player = gamesCupboardClient.findPlayer(i + 1)
            val playerName = Text(player?.name ?: "vacant", color = gamesCupboardClient.playerColor(i)).apply {
                x = if (hasBidColumn) {
                    marginX + i * 2 * (columnWidth + spacing) + (columnWidth * 2 - width) / 2
                } else {
                    marginX + i * (columnWidth + spacing) + (columnWidth - width) / 2
                }
                y = 6.0
            }
            playerNames.add(playerName)
            panel.addChild(playerName)
            if (hasBidColumn) {
                panel.uiText("Bid") {
                    x = marginX + (j - 1) * (columnWidth + spacing)
                    y = 27.0
                }
                panel.uiText("Score") {
                    x = marginX + j * (columnWidth + spacing)
                    y = 27.0
                }
            }

            totals.text("0", color = Colors.WHITE) {
                x = marginX + j * (columnWidth + spacing)
            }
        }

        addRow()

    }

    private fun addRow(): Row {
        val row = Row()
        row.y = if (rows.isEmpty()) 5.0 else rows.last().y + rowHeight
        rows.add(row)
        scrollArea.addChild(row)
        scrollable.scrollTopRatio = 1.0
        scrollable.scrollLeftRatio = 0.0
        return row
    }

    /**
     * From the RenamePlayer and PlayerJoined messages.
     */
    fun renamePlayer(id: Int, name: String) {
        with(playerNames[id - 1]) {
            text = name
            x = if (hasBidColumn) {
                marginX + (id - 1) * 2 * (columnWidth + spacing) + (columnWidth * 2 - width) / 2
            } else {
                marginX + (id - 1) * (columnWidth + spacing) + (columnWidth - width) / 2
            }
        }
    }

    /**
     * Called from the [ScoreSheetData] message.
     */
    fun update(scores: List<List<Double>>) {
        rows.clear()
        scrollArea.children.clear()
        for (scoreRow in scores) {
            val row = addRow()
            row.update(scoreRow)
        }
        recalculate()
    }

    private fun recalculateAndSend() {
        recalculate()
        ScoreSheetData(rows.map { it.values() }).send()
    }

    private fun recalculate() {
        for (x in 0 until seatCount) {
            var total = 0.0
            for (row in rows) {
                total += row.value(if (hasBidColumn) x * 2 + 1 else x)
            }
            (totals.children[x] as Text).text = total.toStringMax1DP()
        }
    }

    inner class Row : Container() {

        init {
            var tx = 0.0
            for (i in 0 until seatCount) {
                if (hasBidColumn) {
                    textInput("0", width = columnWidth) {
                        x = tx
                        y = 0.0
                    }
                    tx += columnWidth + spacing
                }
                textInput("0", width = columnWidth) {
                    x = tx
                    y = 0.0
                }
                tx += columnWidth + spacing
            }
        }

        fun update(data: List<Double>) {
            for ((x, item) in data.withIndex()) {
                (children[x] as TextInput).text = item.toStringMax1DP()
            }
        }

        fun values() = children.map { (it as TextInput).text.toDoubleOrNull() ?: 0.0 }
        fun value(x: Int): Double = (children[x] as TextInput).text.toDoubleOrNull() ?: 0.0
    }
}

private fun Double.toStringMax1DP(): String {
    val times10 = round(this * 10).toInt()
    return if (times10 % 10 == 0) {
        (times10 / 10).toString()
    } else {
        (times10 / 10.0f).toString()
    }
}
