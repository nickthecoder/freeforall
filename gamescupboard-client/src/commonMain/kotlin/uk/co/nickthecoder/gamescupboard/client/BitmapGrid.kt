package uk.co.nickthecoder.gamescupboard.client

import com.soywiz.korim.bitmap.Bitmap
import com.soywiz.korim.bitmap.BitmapSlice
import com.soywiz.korma.geom.RectangleInt
import uk.co.nickthecoder.gamescupboard.common.Grid

class BitmapGrid(
    bitmap: Bitmap,
    val grid: Grid
) {

    val all = BitmapSlice(bitmap, RectangleInt(0, 0, bitmap.width, bitmap.height), null, null)
    fun slice(x: Int, y: Int): BitmapSlice<Bitmap> {
        val rect = RectangleInt(x * grid.itemWidth, y * grid.itemHeight, grid.itemWidth, grid.itemHeight)
        return all.slice(rect)
    }

}
