package uk.co.nickthecoder.gamescupboard.client

import com.soywiz.korge.view.ClipContainer
import com.soywiz.korge.view.solidRect
import com.soywiz.korim.color.Colors
import com.soywiz.korim.color.RGBA

/**
 * The layer below [PlayingArea]
 *
 * Contains a solid background color, as well as static images.
 */
class BackgroundArea(
    width: Double, height: Double

) : ClipContainer(width, height) {

    private val background = solidRect(width, height, Colors["#33333"])

    var bgColor: RGBA
        get() = background.color
        set(v) {
            background.color = v
        }
}