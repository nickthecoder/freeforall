package uk.co.nickthecoder.gamescupboard.client

/**
 * Open a link in a browser.
 * I want to do this from JVM and JS.
 */
expect object FollowLink {
    fun follow( url : String )
}
