package uk.co.nickthecoder.gamescupboard.client.view

import com.soywiz.korge.view.image
import com.soywiz.korim.bitmap.Bitmap
import com.soywiz.korim.bitmap.BitmapSlice
import com.soywiz.korma.geom.Point

open class GridImageObjectView(id: Int, bitmap: BitmapSlice<Bitmap>) : GameObjectView(id) {

    protected val image = image(bitmap) {
        x = -width / 2
        y = -height / 2
    }

    override fun isTouching(point: Point): Boolean {
        return point.x >= x - image.width / 2 && point.x <= x + image.width / 2 &&
                point.y >= y - image.height / 2 && point.y <= y + image.height / 2
    }
}
