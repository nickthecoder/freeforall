package uk.co.nickthecoder.gamescupboard.client

/**
 * I need to pass a gameId into KorGE, but KorGE doesn't allow that (in general).
 *
 * So, In the browser, we get parameters from the "search" (aka query) of the window's location.
 * Using JVM, we must use the ``Launch`` entry point, and it remembers the command line arguments
 * e.g. java ... Launch --gameId=1
 */
expect class Parameters {
    companion object {
        val host: String
        val port: Int
        operator fun get(name: String): String?
    }
}
