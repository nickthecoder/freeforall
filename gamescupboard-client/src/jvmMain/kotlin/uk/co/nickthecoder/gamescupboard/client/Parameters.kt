package uk.co.nickthecoder.gamescupboard.client

/**
 * [Launch] passes the command line arguments to [prepare].
 * Each named parameter must be in the form --NAME=VALUE
 *
 * However, you can also use a single minus, or none at all!
 */
actual class Parameters {

    actual companion object {

        private val map = mutableMapOf<String, String>()

        internal fun prepare(vararg args: String) {

            for (arg in args) {
                val eq = arg.indexOf('=')
                if (eq > 0) {
                    val name = if (arg.startsWith("--")) {
                        arg.substring(2, eq)
                    } else if (arg.startsWith("-")) {
                        arg.substring(1, eq)
                    } else {
                        arg.substring(0, eq)
                    }
                    map[name] = arg.substring(eq + 1)
                }
            }
        }

        actual val host
            get() = get("host") ?: "localhost"

        actual val port
            get() = get("port")?.toIntOrNull() ?: 80

        actual operator fun get(name: String) = map[name]
    }
}