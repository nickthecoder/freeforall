package uk.co.nickthecoder.gamescupboard.client

import java.awt.Desktop
import java.net.URI

actual object FollowLink {

    actual fun follow(url: String) {
        try {
            if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                Desktop.getDesktop().browse(URI(url))
                return
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        // Ok, that didn't work, let's try the unix only alternative
        // FYI, the above never works for me. Grr.
        // Java is broken! And I didn't find a Kotlin solution either.
        try {
            Runtime.getRuntime().exec("xdg-open $url")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
