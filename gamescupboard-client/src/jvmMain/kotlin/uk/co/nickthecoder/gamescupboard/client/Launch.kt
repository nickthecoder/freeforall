package uk.co.nickthecoder.gamescupboard.client

import kotlinx.coroutines.runBlocking

object Launch {

    @JvmStatic
    fun main(vararg args: String) {

        println("Launch")
        Parameters.prepare(*args)

        runBlocking {
            uk.co.nickthecoder.gamescupboard.client.main()
        }
    }

}
