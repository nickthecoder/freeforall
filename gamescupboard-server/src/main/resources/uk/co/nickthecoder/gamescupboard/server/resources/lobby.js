function showError( n, ele, text ) {
    ele.focus()
    const error = document.getElementById( "error" + n );
    error.style.display = "block";
    error.innerHTML = text;
    return false;
}
function checkGameValues( n, minPlayers, maxPlayers ) {
    const label = document.getElementById( "gameLabel" + n );
    if (label.value.trim() == "") {
        return showError( n, label, "Cannot be blank" );
    }
    if (label.value.match( /[^a-z0-9 ]/gi )) {
        return showError( n, label, "Only letters, number and spaces" );
    }
    const players = document.getElementById( "players" + n );
    if ( players.value.match( /[^0-9]/g )) {
        return showError( n, players, "Expected a number" );
    }
    const p = 0 + players.value
    if ( p < minPlayers || p > maxPlayers ) {
        return showError( n, players, "Between " + minPlayers + " and " + maxPlayers );
    }
    return true
}

function genInvitationCode() {
    var code = "";
    for ( var i = 0; i < 6; i ++ ) {
        code += String.fromCharCode( Math.floor(Math.random()*26 + 65) );
    }
    document.getElementById( "invitationCode" ).value = code;
}
