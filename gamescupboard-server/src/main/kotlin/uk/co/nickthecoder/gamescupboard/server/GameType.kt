package uk.co.nickthecoder.gamescupboard.server

import uk.co.nickthecoder.gamescupboard.server.games.*

data class GameType(
    val name: String,
    val label: String,
    val thumbnail: String,
    val variations: List<GameVariation>
)

val gameTypes = listOf(
    bananagrams, cards, chess, connect4, hex, qwirkle, scrabble, super7
).sortedBy { it.name }
