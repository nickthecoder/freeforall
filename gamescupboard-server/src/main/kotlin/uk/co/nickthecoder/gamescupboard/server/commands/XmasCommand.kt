package uk.co.nickthecoder.gamescupboard.server.commands

import AddObjects
import uk.co.nickthecoder.gamescupboard.common.CommandInfo
import uk.co.nickthecoder.gamescupboard.common.ImageObject
import uk.co.nickthecoder.gamescupboard.common.playingAreaHeight
import uk.co.nickthecoder.gamescupboard.common.playingAreaWidth
import uk.co.nickthecoder.gamescupboard.server.ConnectedPlayer

object XmasCommand : Command(CommandInfo("xmas", "Adds Christmas decorations")) {
    override suspend fun run(from: ConnectedPlayer, parameters: List<String>): String? {
        val game = from.game
        val objects = listOf(
            "baubles1", "holly1", "merry1", "reindeer1", "reindeer2", "santa1", "santa2", "snowman1", "tree1"
        ).map {
            ImageObject(
                game.generateObjectId(),
                playingAreaWidth / 2,
                playingAreaHeight / 2,
                draggable = true,
                path = "xmas/$it.png"
            ).apply {
                isScalable = true
            }
        }
        game.add(objects)
        game.send(AddObjects(objects))
        return null
    }
}
