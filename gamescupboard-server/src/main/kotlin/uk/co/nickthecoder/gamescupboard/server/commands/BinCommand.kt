package uk.co.nickthecoder.gamescupboard.server.commands

import AddBin
import uk.co.nickthecoder.gamescupboard.common.CommandInfo
import uk.co.nickthecoder.gamescupboard.server.ConnectedPlayer

/**
 * See PlayingArea.bin and PlayingArea.onMouseUp for how the bin works.
 */
object BinCommand : Command(CommandInfo("bin", "Adds a rubbish bin, where you can throw away any draggable item")) {
    override suspend fun run(from: ConnectedPlayer, parameters: List<String>): String? {
        from.game.send(AddBin)
        return null
    }
}
