package uk.co.nickthecoder.gamescupboard.server.commands

import uk.co.nickthecoder.gamescupboard.common.CommandInfo
import uk.co.nickthecoder.gamescupboard.common.FlippableImageObject
import uk.co.nickthecoder.gamescupboard.common.SpecialPoint
import uk.co.nickthecoder.gamescupboard.server.ConnectedPlayer

class ReplaceCommand(
    val to: SpecialPoint,
    val fromPoints: List<SpecialPoint>
) : Command(
    CommandInfo(
        "replace",
        "Place a pile back into the deck, and shuffle the deck",
        1,
        parameterHelp = listOf("Which pile to replace (0=Left most pile)")
    )
) {

    override suspend fun run(from: ConnectedPlayer, parameters: List<String>): String? {
        val n = parameters[0].toIntOrNull() ?: return "Pile number not specified"
        if (n < 0 || n >= fromPoints.size) return "Invalid pile. Expected range 0..${fromPoints.size}, but found $n"
        val point = fromPoints[n]
        val game = from.game
        val cardsFromPile = game.playingObjects().filter { it.x == point.x && it.y == point.y }
        for (card in cardsFromPile) {
            card.x = to.x
            card.y = to.y
            if (card is FlippableImageObject) {
                card.isFaceUp = false
            }
        }
        shuffleCardsAtPoint(game, to)
        return null
    }
}
