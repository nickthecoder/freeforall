package uk.co.nickthecoder.gamescupboard.server.commands

import AddObjects
import uk.co.nickthecoder.gamescupboard.common.CommandInfo
import uk.co.nickthecoder.gamescupboard.common.ImageObject
import uk.co.nickthecoder.gamescupboard.common.playingAreaHeight
import uk.co.nickthecoder.gamescupboard.common.playingAreaWidth
import uk.co.nickthecoder.gamescupboard.server.ConnectedPlayer

object BirthdayCommand : Command(CommandInfo("birthday", "Adds birthday decorations")) {
    override suspend fun run(from: ConnectedPlayer, parameters: List<String>): String? {
        val game = from.game
        val objects = listOf(
            "balloons", "banner", "cake", "popper", "presents"
        ).map {
            ImageObject(
                game.generateObjectId(),
                playingAreaWidth / 2,
                playingAreaHeight / 2,
                draggable = true,
                path = "birthday/$it.png"
            ).apply {
                isScalable = true
            }
        }
        game.add(objects)
        game.send(AddObjects(objects))
        return null
    }
}
