package uk.co.nickthecoder.gamescupboard.server.commands

import uk.co.nickthecoder.gamescupboard.common.*
import uk.co.nickthecoder.gamescupboard.server.ConnectedPlayer

object LabelCommand :
    Command(
        CommandInfo(
            "label",
            "Adds a draggable label. If you specify more than one word, click the label to cycle through them",
            1,
            10,
            parameterHelp = listOf("The text for this label")
        )
    ) {

    override suspend fun run(from: ConnectedPlayer, parameters: List<String>): String? {

        val label = if (parameters.size == 1 && parameters[0] == "PLAYERS") {
            // A cyclic label with the players' names.
            val names = from.game.connectedPlayers.values.map { it.player.name }
            if (names.isEmpty()) return "No players found"
            TextObject(
                from.game.generateObjectId(),
                playingAreaWidth / 2,
                playingAreaHeight / 2,
                draggable = true,
                style = TextStyle.WHITE_BOX,
                text = names.first()
            ).apply {
                cyclicText.addAll(names)
            }
        } else {
            // A cyclic label from the parameter values.

            TextObject(
                from.game.generateObjectId(),
                playingAreaWidth / 2,
                playingAreaHeight / 2,
                draggable = true,
                style = TextStyle.WHITE_BOX,
                text = parameters[0]
            ).apply {

                if (parameters.size > 1) {
                    cyclicText.addAll(parameters)
                }
            }
        }

        from.game.add(label)

        from.game.send(label.newMessage())
        return null
    }
}
