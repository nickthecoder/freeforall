package uk.co.nickthecoder.gamescupboard.server.commands

import ChatMessage
import MoveObject
import uk.co.nickthecoder.gamescupboard.common.CommandInfo
import uk.co.nickthecoder.gamescupboard.common.SpecialPoint
import uk.co.nickthecoder.gamescupboard.server.ConnectedPlayer

/**
 * Similar to deal, but each player is given a stack of cards, face down on the table.
 */
class StacksCommand(
    private val fromPoint: SpecialPoint,
    private val toPoints: List<SpecialPoint>

) : Command(
    CommandInfo(
        "stacks",
        "Deal cards face down to each player's stack",
        1,
        parameterHelp = listOf("Number of cards to each player")
    )
) {

    override suspend fun run(from: ConnectedPlayer, parameters: List<String>): String? {
        val count = parameters[0].toIntOrNull() ?: return "Expected an integer"
        val game = from.game

        val available = game.playingObjects().count { fromPoint.contains(it.x, it.y) }
        if (available < count * game.connectedPlayers.size) {
            return "Not enough cards. Only $available remaining"
        }

        for (playerId in game.connectedPlayers.keys) {
            val toPoint = toPoints[(playerId - 1 + toPoints.size) % toPoints.size]
            val cards = game.playingObjects().filter { fromPoint.contains(it.x, it.y) }.take(count)
            for (card in cards) {
                game.send(MoveObject(card.id, toPoint.x, toPoint.y, ChangeZOrder.TOP))
                card.x = toPoint.x
                card.y = toPoint.y
            }
        }
        game.send(ChatMessage(from.id, "Dealt stacks of $count cards", null))

        return null
    }
}
