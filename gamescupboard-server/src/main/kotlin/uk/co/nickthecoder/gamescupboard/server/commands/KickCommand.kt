package uk.co.nickthecoder.gamescupboard.server.commands

import io.ktor.websocket.*
import uk.co.nickthecoder.gamescupboard.common.CommandInfo
import uk.co.nickthecoder.gamescupboard.server.ConnectedPlayer

object KickCommand :
    Command(CommandInfo("kick", "Remove a player from the game", 1, parameterHelp = listOf("Player Id (Int)"))) {
    override suspend fun run(from: ConnectedPlayer, parameters: List<String>): String? {

        val game = from.game
        if (from.id != 0) return "Only the game creator can kick players"

        val playerId = parameters[0].toIntOrNull() ?: "Expected a player id (a number)"

        val player = game.connectedPlayers[playerId]
        if (player == null) {
            return "Player #$playerId not found"
        } else {
            player.session.close(CloseReason(1, "Kicked"))
        }

        return null
    }
}
