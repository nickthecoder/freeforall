package uk.co.nickthecoder.gamescupboard.server.games

import uk.co.nickthecoder.gamescupboard.common.*
import uk.co.nickthecoder.gamescupboard.server.FixedAvatarPositions
import uk.co.nickthecoder.gamescupboard.server.GameType
import uk.co.nickthecoder.gamescupboard.server.GameVariation
import uk.co.nickthecoder.gamescupboard.server.Vector2i

private const val boardWidth = 548
private const val left = (playingAreaWidth - boardWidth) / 4
private const val right = playingAreaWidth - left
private const val avatarY = 500
private const val pointY = avatarY - 70

private val redPoint = SpecialPoint("redCounters", left, pointY)
private val yellowPoint = SpecialPoint("yellowCounters", right, pointY)

private val boardArea = SpecialArea(
    "boardArea", AreaType.PUBLIC, 149, 83, 507, 438,
    RectangularSnapToGrid(507 / 7, 438 / 6)
)

val normalConnect4Variation = GameVariation(
    "connect4",
    "Connect 4",
    minPlayers = 2,
    maxPlayers = 2,
    grids = listOf(),
    backgroundObjects = listOf(
        ImageObject(-1, 400, 300, path = "backgrounds/purple.jpg", draggable = false),
    ),
    playingObjects = listOf(
        ImageObject(0, redPoint.x, redPoint.y, path = "connect4Red.png").apply { regen = true },
        ImageObject(1, redPoint.x, redPoint.y, path = "connect4Red.png").apply { regen = true },
        ImageObject(2, yellowPoint.x, yellowPoint.y, path = "connect4Yellow.png").apply { regen = true },
        ImageObject(3, yellowPoint.x, yellowPoint.y, path = "connect4Yellow.png").apply { regen = true }
    ),
    foregroundObjects = listOf(
        ImageObject(-2, 400, 300, path = "connect4Board.png", draggable = false)
    ),
    specialAreas = listOf(boardArea),
    specialPoints = listOf(redPoint, yellowPoint),
    avatarPositions = FixedAvatarPositions(
        listOf(
            Vector2i(left, avatarY),
            Vector2i(right, avatarY)
        )
    ),
    playerColors = listOf(GameVariation.red, GameVariation.yellow)
)

val connect4 = GameType(
    "connect4", "Connect 4", "connect4.png",
    listOf(normalConnect4Variation, gomoku)
)
