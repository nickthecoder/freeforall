package uk.co.nickthecoder.gamescupboard.server

import io.ktor.server.html.*
import kotlinx.html.*

class LobbyTemplate : Template<HTML> {

    val pageTitle = Placeholder<TITLE>()
    val pageHeading = Placeholder<FlowContent>()
    val pageFooting = Placeholder<FlowContent>()
    val content = Placeholder<FlowContent>()


    override fun HTML.apply() {

        head {
            title { insert(pageTitle) }
            meta(name = "viewport", content = "width=device-width, initial-scale=1")
            link(rel = "stylesheet", type = "text/css", href = "/resources/style.css")
            link(rel = "icon", href = "/resources/icon.png")
            script(src = "/resources/lobby.js") {}
        }

        body {
            h1 { insert(pageHeading) }

            div(classes = "content") {
                insert(content)
            }

            div(classes = "footing") {
                insert(pageFooting)
            }
        }
    }

}
