package uk.co.nickthecoder.gamescupboard.server.commands

import ChatMessage
import RemoveObjects
import uk.co.nickthecoder.gamescupboard.common.CommandInfo
import uk.co.nickthecoder.gamescupboard.common.FlippableImageObject
import uk.co.nickthecoder.gamescupboard.server.ConnectedPlayer

private val suitNames = listOf("Clubs", "Diamonds", "Hearts", "Spades")

class RemoveCardCommand :
    Command(CommandInfo("remove", "Remove a card", 1, parameterHelp = listOf("Card Name e.g. AH, 2C, QS, KD"))) {
    override suspend fun run(from: ConnectedPlayer, parameters: List<String>): String? {

        val p = parameters[0].uppercase()
        if (p.length != 2) {
            return "Expected a card and suit. e.g. 2H"
        }
        val n = if (p[0] == 'A') 0 else "1234567890JQK".indexOf(p[0])
        if (n < 0) return "Expected the 1st character to be card number: 1234567890JQKA"

        val suit = "CDHS".indexOf(p[1])
        if (suit < 0) return "Expected the 2nd character to a suit: CDHS"

        val game = from.game

        val card = game.playingObjects().firstOrNull {
            it is FlippableImageObject && it.gx == n && it.gy == suit
        } ?: return "Card not found"

        game.remove(card)
        game.send(RemoveObjects(listOf(card.id)))
        game.send(ChatMessage(from.id, "Removed ${p[0]} ${suitNames[suit]}"))
        return null
    }
}
