package uk.co.nickthecoder.gamescupboard.server.commands

import uk.co.nickthecoder.gamescupboard.common.CommandInfo

object Commands {

    private val commandsByName = mutableMapOf<String, Command>()

    val standardCommandInfos: List<CommandInfo> get() = commandsByName.values.map { it.info }

    fun addAll(vararg commands: Command) {
        for (command in commands) {
            commandsByName[command.name] = command
        }
    }

    fun find(commandName: String): Command? = commandsByName[commandName]

    init {
        addAll(RenameCommand, ResetCommand, LabelCommand, KickCommand, BinCommand, BirthdayCommand, XmasCommand)
    }
}
