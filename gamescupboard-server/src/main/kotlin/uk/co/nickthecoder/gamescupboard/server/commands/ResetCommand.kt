package uk.co.nickthecoder.gamescupboard.server.commands

import uk.co.nickthecoder.gamescupboard.common.CommandInfo
import uk.co.nickthecoder.gamescupboard.server.ConnectedPlayer

object ResetCommand : Command(CommandInfo("reset", "Reset the board ready for a new game")) {
    override suspend fun run(from: ConnectedPlayer, parameters: List<String>): String? {

        val game = from.game
        game.reset()

        for (c in game.gameVariation.commands) {
            if (c.isInitialiser) {
                c.run(from, emptyList())
            }
        }

        return null
    }
}
