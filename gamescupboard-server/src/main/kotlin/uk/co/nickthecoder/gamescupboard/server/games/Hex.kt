package uk.co.nickthecoder.gamescupboard.server.games

import uk.co.nickthecoder.gamescupboard.common.*
import uk.co.nickthecoder.gamescupboard.server.*

private val redPoint = SpecialPoint("redCounters", 737, 195)
private val bluePoint = SpecialPoint("redCounters", 800 - 737, 600 - 195)
private val hexArea = SpecialArea(
    "hexGrid", AreaType.PUBLIC, 7, 67, 800, 75 + 42 * 11,
    HexSnap(49.3, 42.7)
)

val normalHexVariation = GameVariation(
    "hex",
    "Hex",
    minPlayers = 2,
    maxPlayers = 2,

    grids = listOf(),

    backgroundObjects = listOf(
        ImageObject(-1, 400, 300, path = "backgrounds/khaki.jpg", draggable = false),
        ImageObject(-2, 400, 300, path = "hexBoard.png", draggable = false)
    ),

    playingObjects = listOf(
        ImageObject(0, redPoint.x, redPoint.y, path = "hexRed.png").apply { regen = true },
        ImageObject(1, redPoint.x, redPoint.y, path = "hexRed.png").apply { regen = true },
        ImageObject(2, bluePoint.x, bluePoint.y, path = "hexBlue.png").apply { regen = true },
        ImageObject(3, bluePoint.x, bluePoint.y, path = "hexBlue.png").apply { regen = true }
    ),

    specialAreas = listOf(hexArea),

    specialPoints = listOf(redPoint, bluePoint),

    avatarPositions = FixedAvatarPositions(
        listOf(
            Vector2i(playingAreaWidth - 70, playingAreaHeight - 500),
            Vector2i(70, 500)
        ),
        listOf(0, 2)
    )
)

val hex = GameType(
    "hex", "Hex", "hex.png",
    listOf(normalHexVariation)
)
