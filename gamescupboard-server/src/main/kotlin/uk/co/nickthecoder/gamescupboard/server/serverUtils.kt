package uk.co.nickthecoder.gamescupboard.server

import Message
import io.ktor.websocket.*
import kotlinx.html.TABLE
import kotlinx.html.TD
import kotlinx.html.td
import kotlinx.html.tr
import java.util.*

suspend fun WebSocketSession.send(message: Message) {
    try {
        send(message.toJsonPacket())
    } catch (e: Throwable) {
        println("Error while sending message")
        e.printStackTrace()
        println("Message = $message")
    }
}

fun TABLE.row(label: String, block: TD.() -> Unit) {
    tr {
        td { +label }
        td(block = block)
    }
}

fun TABLE.row(label: String, info: String, block: TD.() -> Unit) {
    row(label, info = { +info }, block = block)
}

fun TABLE.row(label: String, info: TD.() -> Unit, block: TD.() -> Unit) {
    tr {
        td(classes = "withInfo") { +label }
        td(classes = "withInfo", block = block)
    }
    tr {
        td(classes = "info") {
            colSpan = "2"
            info()
        }
    }
}

fun TABLE.row(block: TD.() -> Unit) {
    tr {
        td {
            colSpan = "2"
            block()
        }
    }
}

fun Date.minutesAgo(): Long {
    val seconds = (Date().time - this.time) / 1000
    return seconds / 60
}

fun Date.secondsAgo(): Long {
    return (Date().time - this.time) / 1000
}

fun Date.ago(): String {
    val seconds = (Date().time - this.time) / 1000
    val minutes = seconds / 60
    val hours = minutes / 60
    return if (seconds < 60) {
        "active"
    } else if (minutes == 1L) {
        "1 minute ago"
    } else if (minutes < 60) {
        "$minutes minutes ago"
    } else if (hours == 1L) {
        "1 hour ago"
    } else if (hours < 24) {
        "$hours hours ago"
    } else {
        "Days ago"
    }
}
