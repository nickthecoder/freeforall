package uk.co.nickthecoder.gamescupboard.server.games

import uk.co.nickthecoder.gamescupboard.common.*
import uk.co.nickthecoder.gamescupboard.server.FixedAvatarPositions
import uk.co.nickthecoder.gamescupboard.server.GameVariation
import uk.co.nickthecoder.gamescupboard.server.Vector2i

private const val stoneSize = 22
private const val left = 40
private const val top = 40

private val blackPoint = SpecialPoint("blackStones", 710, 140)
private val whitePoint = SpecialPoint("whiteStones", 710, 600 - 140)

private val avatarPositions = FixedAvatarPositions(
    listOf(
        Vector2i(playingAreaWidth - 90, 100),
        Vector2i(playingAreaWidth - 90, playingAreaHeight - 100)
    ),
    listOf(2, 0)
)

/**
 * The main playing area. Tiles are always face up here.
 */
private val playArea = SpecialArea(
    "tileGrid",
    AreaType.PUBLIC,
    400 - 10 * stoneSize + stoneSize / 2,
    300 - 10 * stoneSize + stoneSize / 2,
    19 * stoneSize,
    top + 19 * stoneSize,
    changeZOrder = ChangeZOrder.TOP,
    snap = RectangularSnapToGrid(stoneSize, stoneSize)
)

val gomokuRules = """
    _Gomoku_
    
    Gomoku is played on a Go board, but is very similar to Connect 4 (without gravity).
    
    Players take it in turns to place a stone on any unoccupied intersection.
    
    The aim of the game is to create a line of 5 stones either
    horizontally, vertically or diagonally.
    In some variations, the line must be _exactly_ 5 stone long,
    but in others, lines of 6 or more are also winning.
    
    If the board is filled, so that no winning lines are possible,
    the game is a draw.
    
    For more information, see the [Wikipedia article|https://en.wikipedia.org/wiki/Gomoku].
    
    """.trimIndent()
val gomoku = GameVariation(
    "gomoku",
    "Gomoku",
    minPlayers = 2,
    maxPlayers = 2,

    grids = emptyList(),

    backgroundObjects = listOf(
        ImageObject(- 1, 400, 300, path = "backgrounds/khaki.jpg", draggable = false),
        ImageObject(- 2, 400, 300, path = "goBoard.jpg", draggable = false)
    ),

    playingObjects = listOf(
        ImageObject(0, blackPoint.x, blackPoint.y, path = "goStoneBlack.png").apply { regen = true },
        ImageObject(1, whitePoint.x, whitePoint.y, path = "goStoneWhite.png").apply { regen = true }
    ),

    specialAreas = listOf(playArea),
    specialPoints = listOf(blackPoint, whitePoint),
    avatarPositions = avatarPositions,
    rules= gomokuRules
)
