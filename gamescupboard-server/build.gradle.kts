val ktor_version: String by project
val kotlin_version: String by project

val serverMainClass = "uk.co.nickthecoder.gamescupboard.server.GamesCupboardServer"

project.setProperty("mainClassName", serverMainClass)

plugins {
    application
    kotlin("jvm")
    kotlin("plugin.serialization")
}

application {
    mainClass.set(serverMainClass)
}

repositories {
    mavenCentral()
    mavenLocal()
}

dependencies {
    implementation(project(":gamescupboard-common"))
    implementation("io.ktor:ktor-server-core:$ktor_version")
    implementation("io.ktor:ktor-server-websockets:$ktor_version")

    implementation("io.ktor:ktor-server-netty:$ktor_version")
    implementation("io.ktor:ktor-server-html-builder:$ktor_version")
    implementation("io.ktor:ktor-server-status-pages:$ktor_version")
    implementation("io.ktor:ktor-server-auth:$ktor_version")

    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.4.1")
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.4.0")
    implementation("org.slf4j:slf4j-nop:2.0.5") // Disable logging

}
